# Mögliche Themen 

 

## IoT 


- Hardware Device (oder simuliertes Device) welches Daten über JMS an Data Client sendet 
- Daten können vom Data Client gespeichert und analysiert werden 




### Automation with Java Realtime Environment 


- Einfache Maschinenautomation anstatt mit konventiollen PLCs über Low Cost Hardware Varianten mit Hilfe OOP Java   
- Hardware Device auf welchem Java Realtime Environment läuft  

Bespiel: Raspberry PI 

- Hauptaufgabe: Erstellung eines einfachen Basis Frameworks welches grundlegende Funktionalitäten für z.B. Statemachine und Parameter  Handling bereitstellt 

- Erstellung einer sehr einfachen Bespielapplikation mit diesem Framework 

  ​


#### Issues

- State (Class)
- Parameter (Class)
- Current value (Class)
- Mode (Class)
- Logging
- Command (Method)
- Common interface
- Message Provider
- Specific interface
- Base controller
- Observer pattern GUI

### Network Monitoring 


- Tool zur Überwachung von Netzwerkteilnehmer 
- Einfaches Verfahren über Netzwerk Ping anwenden 
- Konfiguration der Devices über UI 




## Tools 

 

| No   | Tool                | Version                 | Link                                     | Used |
| ---- | ------------------- | ----------------------- | ---------------------------------------- | ---- |
| 1    | JRE                 | 1.8.0_111               | [http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) | Ja   |
| 2    | Eclipse             | Neon.1a Release (4.6.1) | [https://eclipse.org/neon](https://eclipse.org/neon) | Ja   |
| 3    | Maven               |                         | Included in Neon package                 | Ja   |
| 4    | Spring              |                         |                                          | Nein |
| 5    | Papyrus UML         |                         | [https://wiki.eclipse.org/Papyrus-RT/User/User_Guide/Installation](https://wiki.eclipse.org/Papyrus-RT/User/User_Guide/Installation) [https://wiki.eclipse.org/Papyrus_User_Guide](https://wiki.eclipse.org/Papyrus_User_Guide) [http://wiki.eclipse.org/Java_Code_Generation](http://wiki.eclipse.org/Java_Code_Generation) | Ja   |
| 6    | BitBucket           |                         | https://bitbucket.org                    | Ja   |
| 7    | JUnit               | V4.x                    |                                          | Ja   |
| 8    | Mockito             |                         | [http://site.mockito.org/](http://site.mockito.org/) | Ja   |
| 9    | Windows Builder Pro |                         | [http://www.eclipse.org/windowbuilder/download.php](http://www.eclipse.org/windowbuilder/download.php) | Ja   |
| 10   | OneNote             |                         |                                          | Nein |
| 11   | JMS                 |                         |                                          | ?    |
| 12   | LaTex               |                         | [http://texlipse.sourceforge.net/](http://texlipse.sourceforge.net/) (Eclipse Plugin for LaTex). Install with Eclipse Marketplace. | Nein |
| 13   | Typora              |                         | http://www.typora.io/ (for Drafts, supports Eclipse support) | Ja   |
| 14   | Egit                |                         | http://download.eclipse.org/egit/updates | Ja   |
|      |                     |                         |                                          |      |





## Sprachelemente 

 

| No   | Element            | Comment                                  | Link                                     | Used |
| ---- | ------------------ | ---------------------------------------- | ---------------------------------------- | ---- |
| 1    | Lambda             |                                          | [https://blog.codecentric.de/2013/10/java-8-erste-schritte-mit-lambdas-und-streams/](https://blog.codecentric.de/2013/10/java-8-erste-schritte-mit-lambdas-und-streams/) |      |
| 2    | JUnit              | White-Box Test(stub / dummy / mock)      |                                          |      |
| 3    | Interfaces         |                                          |                                          |      |
| 4    | Design Patterns    | MVC Singleton Factory Observer State Pattern Command Pattern | [https://www.tutorialspoint.com/design_pattern/](https://www.tutorialspoint.com/design_pattern/) |      |
| 5    | Serialisierung     |                                          |                                          |      |
| 6    | Multi Threading    |                                          |                                          |      |
| 7    | Exception Handling |                                          |                                          |      |
| 8    | GUI                |                                          |                                          |      |
| 9    | Generics           |                                          |                                          |      |
| 10   | File IO            |                                          |                                          |      |
| 11   | Network            |                                          |                                          |      |
| 12   | Collections        |                                          |                                          |      |
| 13   |                    |                                          |                                          |      |
| 14   |                    |                                          |                                          |      |
| 15   |                    |                                          |                                          |      |



## Raspberry Pi Implementation  

- Interfaces zur Automatisierungswelt 
  - OPC
     U/A als Klemme (Echtzeit Spezifikation soll dieses Jahr noch 
    veröffentlicht werden). Es ist somit denkbar, dass auch IO's und Drives 
    über OPC U/A zu implementieren (Zukunftsvision). 
    [https://www.br-automation.com/de-ch/unternehmen/presse/das-erste-feldgerat-mit-opc-ua-und-pubsub/](https://www.br-automation.com/de-ch/unternehmen/presse/das-erste-feldgerat-mit-opc-ua-und-pubsub/) 
  - ModBus TCP offen spezifiziert und über Ethernet relativ einfach einzubinden (K-Klemmen), beschränkte Echtzeitfähigkeiten (für viele Anwendungen jedoch genügend). 
  - Codesys control for raspberry pi (leider kein Ansatz für eine direkte Schnittstelle zu Java gefunden, reines Pointerinterface mit Java schwierig) 
    [http://store.codesys.com/codesys-control-for-raspberry-pi-sl.html](http://store.codesys.com/codesys-control-for-raspberry-pi-sl.html) 
  - GPIO direkte IO's von Raspberry verwenden und auf externe IO's vorerst verzichten 
  - Jamaica VM ;-), waren dieses Jahr leider nicht in Nürnberg und auf der Homepage habe ich keine Infos zu möglichen Hardwareanbindung gefunden. Kommen wohl mehr aus der Embedded Welt. 
    https://www.aicas.com/cms/en/JamaicaVM 
  - Ansatz mit echtzeitfähigem Driverlevel in Codesys Umgebung.
    - EtherCat Master auf Raspberry Pi mit Codesys Control on raspberry Pi
    - Driver Level als strukturierten Text implementiert (CoDeSys)
    - Alle Controller ab Level 1 werden in Java implementiert (nicht echtzeitfähig)
  - Rest Client mit Raspberry Pi

## Maven

- New Maven project
- create a simple project
- Siehe Maven "Hello World"
- Dependencies werden automatisch eingefügt
- ​