## Anforderungen Controller

- Basis Statemaschine wird Vererbt
  - Kommandierungsmethode
    - eigene Methode für jedes Kommando (keine Standardisierung möglich?)
    - Wird ein Kommando mit Enable/Disable gesteuert? oder erst beim Aufruf entschieden ob Kommando abgesetzt werden darf oder nicht?
  - State selber ist ein enum
    - Erweiterung möglich
  - Schnittstelle für ein Leitsystem oder globalen Koordinator, damit Kommandierung nicht nur vom Objekt das die Instanz anlegt kommandiert werden kann sondern auch global (Modus Manu)
  - Modewechsel auch aufgrund von anderen Events (Not-Halt, Störung, IO's, ...)
- Parameter handling (darf ein Parameter eine Klasseninstanz sein deren Methoden Public von einer anderen Klasse aufgerufen werden)
- current value handling
- logging states

### Fazit

- Controller werden in einer Objektorientierten Welt feiner skaliert sein als bisher in der AVM (evtl. haben minisubcontroller ein beschränktes Interface)
- https://github.com/Ankzz/easyfsm -> genauer anschauen

## Offene Punkte Statemachine

- Basisfrage: ist State ein enum, Objekt, Interface oder eine Methode, ...
- Wie diametral stehen diese Anfoderungen einer Objektorientiertem Ansatz im Weg:
- Zur Verfügung stehende States einer Statemaschine sollen per Auswahl während dem codieren zur Verfügung stehen.
- Entwickler schreibt einen Stateübergang zu State X der in Statemaschine gar nicht zur Verfügung steht soll compiler dies erkennen (keine Laufzeitfehler).
- Was ist aber wenn ein State zur Laufzeit erweitert werden kann (Compiler kann Code nicht verifizieren), ein Fall für UnitTest alle Stateübergange testen Implementiermöglichkeiten.
- States sollen eindeutig sein, dies benötigt einen Enum oder genügt eine Arrayliste der states
- Controller mit Interface wäre der Ansatz damit Controller eine Statemaschine und Parameter vererbar wäre, nur ein Interface besitzt keinen Code?

enum Erweiterbar mit Vererbung?

```java
public enum eBaseStates {
	PowerOn ("Power on"),
	Off ("Off"),
	Run ("Run");
    private final String name;   // in kilograms
    eBaseStates(String name) {
        this.name = name;
    }
}
```



```java
public enum eStates implements eBaseStates{
	Moving,
}
```