# Java Statemachine in der Automatisierungstechnik

## Einführung

In der Automatisierungstechnik ist Java ohne bis heute kaum zum Einsatz gekommen. Die Hindernisse für den Einsatz von Java sind:

- Laufzeitfehler sollen nicht auftreten (Dynamische Allozierung von Speicher wird selten genutzt)
- Tasks werden in der SPS zyklisch aufgerufen (Eventorientierte Software ist wenig verbreitet)
- Der Garbage collector von Java verhindert die Echtzeitfähigkeit der Java Virtual Machine

In vielen Automatisierungsapplikationen beschränkt sich die Echtzeitanforderung auf die hardwarenahen Objekte. Die Koordination der verschiedenen Objekte (in unserem Fall Controller genannt) inklusive der Produktverfolung und Koordination verschiedener Rezepturen könnte in einer Java Umgebung jedoch bedeutend einfacher Implementiert werden.

## Ziel des Fallbeispiels

Im Fallbeispiel sollen die Möglichkeiten einer typischen Statemachine aus der Automatisierungstechnik in einer Java Umgebung implementiert werden. Dabei sollen moderne Aspekte der Vererbung, Multithreading, Eventorientierung soweit wie möglich in die Arbeit einfliessen. Folgende Themen sollen teil des Fallbeispiels sein:

- Statemaschine mit einer Entry und Exit Action sowie dem zyklischen Codeaufruf für jeden Status.
- Zu jedem Controller gehören Ein- und Ausgänge, diese werden simuliert und bilden eine mögliche Grundlage für Statewechsel.
- Statewechsel können auch durch Kommandierung (Events) von anderen Controllern initiiert werden.
- Die Kommandierung der Controller und aktuellen Zustände sollen in einer Swing Oberfläche visualisiert werden.

In Abhängigkeit der Fortschritte während der Projektphase gibt es weitere Aspekte die Berücksichtigt werden können:

- Jede Statemachine kann ihr verhalten durch Parameter verändern (Überwachungszeiten, Optionale Sensoren, ...)
- Aktualwerte zur Unterstützung der Fehlersuche können implementiert werden.
- Logging der Statewechsel in ein Logfile.
- Einlesen von Hardwareingängen, mögliche Ansätze:
  - Rest Client auf dem Raspberry Pi
  - Modbus TCP Hardwareklemmen (Beckhoff)
- Definition der Statemachine und deren Übergänge in einer Datei.





